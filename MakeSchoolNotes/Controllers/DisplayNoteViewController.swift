//
//  DisplayNoteViewController.swift
//  MakeSchoolNotes
//
//  Created by Abdelrahman Ibrahim on 6/23/16.
//  Copyright © 2016 MakeSchool. All rights reserved.
//

import UIKit
import RealmSwift

class DisplayNoteViewController: UIViewController {
    
    
    @IBOutlet weak var noteContentTextView: UITextView!
    @IBOutlet weak var noteTitleTextField: UITextField!
    var note: Note?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if let identifier = segue.identifier {
            if identifier == "Cancel" {
                print("Cancel button tapped")
            } else if identifier == "Save" {
                print("Save button tapped")
                
                
                let listNotesTableViewController = segue.destinationViewController as! ListNotesTableViewController
                if segue.identifier == "Save" {
                    if let note = note {
                        let newNote = Note()
                        newNote.title = noteTitleTextField.text ?? ""
                        newNote.content = noteContentTextView.text ?? ""
                        RealmHelper.upadteNote(note, newNote: newNote)
                    }else{
                        let note = Note()
                        note.title = noteTitleTextField.text ?? ""
                        note.content = noteContentTextView.text ?? ""
                        note.modificationDate = NSDate()
                        RealmHelper.addNote(note)
                        listNotesTableViewController.notes = RealmHelper.retrieveNotes()

                    }
                }
                
            }
            
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if let note = note {
            noteTitleTextField.text = note.title
            noteContentTextView.text = note.content
            
        }else {
            noteTitleTextField.text = ""
            noteContentTextView.text  = ""
        }
    }
}
