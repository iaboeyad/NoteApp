//
//  ListNotesTableViewController.swift
//  MakeSchoolNotes
//
//  Created by Abdelrahman Ibrahim on 6/23/16.
//  Copyright © 2016 MakeSchool. All rights reserved.
//

import UIKit
import RealmSwift



class ListNotesTableViewController: UITableViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        notes = RealmHelper.retrieveNotes()
    }
    
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notes.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        //to desplay note title and modification date
        
        let cell = tableView.dequeueReusableCellWithIdentifier("listNotesTableViewCell", forIndexPath: indexPath) as! ListNotesTableViewCell
        
        let row = indexPath.row
        let note = notes[row]
        cell.noteTitleLabel.text = note.title
        //let dataString = String(data: note.modificationDate, encoding: NSUTF8StringEncoding)
        cell.noteModificationTimeLabel.text = note.modificationDate.convertToString()
        
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if let identifire = segue.identifier {
            if identifire == "displayNote" {
                print ("Transetoin to the note view controller")
            }else if identifire == "addNote" {
                print ("+ Button Taped")
            }
            if let identifier = segue.identifier {
                
                if identifier == "displayNote" {
                    print ("table view tapped")
                    
                    let indexPath = tableView.indexPathForSelectedRow!
                    let note = notes[indexPath.row]
                    let displayNoteViewController = segue.destinationViewController as! DisplayNoteViewController
                    displayNoteViewController.note = note
                    
                } else if identifier == "addNote" {
                    print("+ button tapped")
                }
                
            }
        }
        
    }
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            RealmHelper.deleteNote(notes[indexPath.row])
            notes = RealmHelper.retrieveNotes()
//            tableView.reloadData()
        }
    }
    @IBAction func unwindToListNoteviewController(seque: UIStoryboardSegue) {
    }
    
    var notes : Results<Note>! {
        didSet {
            tableView.reloadData()
        }
    }
    
}
    