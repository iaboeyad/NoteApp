//
//  Note.swift
//  MakeSchoolNotes
//
//  Created by Abdelrahman Ibrahim on 22/06/2016.
//  Copyright © 2016 MakeSchool. All rights reserved.
//

import Foundation
import RealmSwift

class Note: Object {
    dynamic var title = ""
    dynamic var content = ""
    dynamic var modificationDate = NSDate ()
    
}